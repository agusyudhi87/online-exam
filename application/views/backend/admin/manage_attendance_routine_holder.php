<div class="col-md-6">
	<div class="form-group">
	<label class="control-label" style="margin-bottom: 5px;">Mata Kuliah</label>
		 <select name="routine_id" id="routine_id" class="form-control selectboxit">
                <?php
                $routine = $this->db
                            ->select("cr.*,s.name subjectname")
                            ->join("subject s", "s.subject_id = cr.subject_id")  
                            ->get_where('class_routine cr',array(
                                'cr.class_id' => $class_id,                               
                                'cr.section_id' => $section_id,
                                'day' => strtolower(date('l',$date))))
                            ->result_array();
                foreach ($routine as $row):
                    ?>
                    <option value="<?php echo $row['class_routine_id']; ?>">
                            <?php echo $row['subjectname']; ?>
                    </option>
                <?php endforeach; ?>
            </select>
	</div>
</div>

<script type="text/javascript">
   
    $(document).ready(function () {

        // SelectBoxIt Dropdown replacement
        if ($.isFunction($.fn.selectBoxIt))
        {
            $("select.selectboxit").each(function (i, el)
            {
                var $this = $(el),
                        opts = {
                            showFirstOption: attrDefault($this, 'first-option', true),
                            'native': attrDefault($this, 'native', false),
                            defaultText: attrDefault($this, 'text', ''),
                        };

                $this.addClass('visible');
                $this.selectBoxIt(opts);
            });
        }

    });

</script>