<div class="col-md-3">
	<div class="form-group">
	<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('section');?></label>
		<select name="semester" id="semester" class="form-control selectboxit"  onchange="select_routine($('#timestamp').val());">

            <?php
            $prodi = $this->db->select("p.*")
                        ->join("prodi p","p.prodi_id=c.prodi_id")
                        ->get_where('class c' , array('c.class_id' => $class_id))
                        ->result_array();

            for ($i=1; $i <= $prodi[0]["semester"]; $i++):
            ?>
           <option value="<?php echo $i;?>"><?php echo $i;?></option>
			<?php endfor;?>
		</select>
	</div>
</div>

<script type="text/javascript">
   
    $(document).ready(function () {

        // SelectBoxIt Dropdown replacement
        if ($.isFunction($.fn.selectBoxIt))
        {
            $("select.selectboxit").each(function (i, el)
            {
                var $this = $(el),
                        opts = {
                            showFirstOption: attrDefault($this, 'first-option', true),
                            'native': attrDefault($this, 'native', false),
                            defaultText: attrDefault($this, 'text', ''),
                        };

                $this.addClass('visible');
                $this.selectBoxIt(opts);
            });
        }

    });

</script>